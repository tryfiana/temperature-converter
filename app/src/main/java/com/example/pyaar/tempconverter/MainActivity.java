package com.example.pyaar.tempconverter;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    RadioGroup radioGroup;
    RadioButton radioButton;
    EditText value;
    Button convert;
    TextView showResultsLabel,showResults;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        value =  (EditText) findViewById(R.id.temp);
        convert = (Button) findViewById(R.id.convert);
        radioGroup = (RadioGroup) findViewById(R.id.temperature);
        showResultsLabel = (TextView) findViewById(R.id.show_result_label);
        showResults = (TextView) findViewById(R.id.converted_temp);

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = value.getText().toString();

                try {
                    double tempInput = Double.parseDouble(input);
                    boolean isValidInput = isValid(input);
                    if (isValidInput) {
                        int radiobuttonid = radioGroup.getCheckedRadioButtonId();
                        radioButton = (RadioButton) findViewById(radiobuttonid);
                        if (radioButton.getText().toString().equals("Celcius to Fahrenheit")) { // celcius to fahrenheit
                            double result = (tempInput * 9 / 5) + 32;
                            showResults.setText(String.valueOf(result));
                            showResultsLabel.setText("Celcius to Fahrenheit");
                        } else {
                            double result = (tempInput - 32) * 5 / 9;
                            showResults.setText(String.valueOf(result));
                            showResultsLabel.setText("Fahrenheit to Celcius");
                        }
                    } else {
                        value.setError("Please enter some numeric values");
                        //Snackbar.make(v, "Please enter a valid input", Snackbar.LENGTH_LONG).show();
                    }

                }catch(NumberFormatException e){
                    value.setError("Please enter numeric values");
                }
                //
            }

        });

    }

    public boolean isValid (String a){
        boolean isValid = true;
        if(a.trim().equals("")){
            isValid = false;
        }
        return isValid;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
